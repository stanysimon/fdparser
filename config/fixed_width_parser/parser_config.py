'''
This config file is used by the fixed_width_parser module to parse a fixed-width file.
'''
CONFIG = {
    'ColumnNames': [
        "id",
        "first_name",
        "last_name",
        "email",
        "gender",
        "ip_address",
        "status",
        "app_version",
        "car_model",
        "car_model_year"
    ],
    'Offsets': [
        5,
        20,
        20,
        50,
        15,
        16,
        5,
        10,
        30,
        5
    ],
    'FixedWidthEncoding': 'cp1252',
    'DelimitedEncoding': 'UTF-8',
    'Padding': '~'
}