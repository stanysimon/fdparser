'''
This config file is used by the fixed_width_generator module to create fixed-width files based on the configuration provided below.
Details: https://pypi.org/project/FixedWidth/
'''
CONFIG = {

     "id": {
        "required": True,
        "type": "string",
        "start_pos": 1,
        "end_pos": 5,
        "alignment": "left",
        "padding": "~"
    },
    "first_name": {
        "required": True,
        "type": "string",
        "start_pos": 6,
        "length": 20,
        "alignment": "left",
        "padding": "~"
    },
    "last_name": {
        "required": True,
        "type": "string",
        "start_pos": 26,
        "length": 20,
        "alignment": "left",
        "padding": "~"
    },

    "email": {
        "required": False,
        "type": "string",
        "start_pos": 46,
        "length": 50,
        "alignment": "left",
        "padding": "~"
    },

    "gender": {
        "type": "string",
        "alignment": "right",
        "start_pos": 96,
        "padding": "~",
        "length": 15,
        "required": True
    },

    "ip_address": {
        "type": "string",
        "start_pos": 111,
        "padding": "~",
        "length": 16,
        "alignment": "left",
        "required": True
    },

    "status": {
        "required": True,
        "type": "string",
        "start_pos": 127,
        "length": 5,
        "alignment": "left",
        "padding": "~"
        },

    "app_version": {
        "required": True,
        "type": "string",
        "start_pos": 132,
        "length": 10,
        "alignment": "right",
        "padding": "~"
        },

    "car_model": {
        "required": True,
        "type": "string",
        "start_pos": 142,
        "length": 30,
        "alignment": "right",
        "padding": "~"
        },

    "car_model_year": {
        "required": True,
        "type": "string",
        "start_pos": 172,
        "length": 5,
        "alignment": "right",
        "padding": "~"
        }
}