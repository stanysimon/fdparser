'''
parser.py

Description: 
Fixed-width is a file format where data is arranged in columns, but instead 
of those columns being delimited by a certain character (as they are in CSV) every row is the exact same length. 
The application reading the file must know how long each column is. There is no way to infer this from the file.

This function makes use of pre-defined configurations provided in the config/fixed_width_parser folder to understand 
the length , encoding and padding for the fixed width file it is about to read.

Parameters:
fixed_width_file_path : Source path to the file
column_widths: Column width of each columns as a list 
column_names: Names of the columns in order as a list
padding: The character used for padding
fixedwidth_encoding: The encoding the source file uses
delimited_encoding: Encoding used to create the CSV file

returns: path to the new generated CSV file
'''
import csv
import os

def generate_csv(fixed_width_file_path = '',column_widths = [], column_names = [],padding = "~",fixedwidth_encoding = '',delimited_encoding = ''):
    '''
    This function reads the config file and the path to the fixed-width file and then returns the path of the CSV file it generates.
    '''
    counter=0
    data = dict()
    with open(fixed_width_file_path,encoding=fixedwidth_encoding) as fd_file:
        csv_file_name = os.path.split(fixed_width_file_path)[1].split(".")[0] + ".csv"
        with open(csv_file_name, mode='w',encoding=delimited_encoding) as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=column_names)
            writer.writeheader()
            while True:
                line = fd_file.readline()
                if not line:
                    break
                body = dict()
                start = 0
                for position in range(len(column_names)):
                    body[column_names[position]] = line[start:(column_widths[position]+start)].strip(padding)
                    start = start + column_widths[position]
                writer.writerow(body)    
                counter = counter+1
        return os.path.abspath(csv_file_name)