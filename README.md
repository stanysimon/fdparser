# Fixed Width Parser  (with sample data generator)

This python project performs the following:
- Parses fixed-width files and generates a CSV ( comma delimited ) file an as output
- Generates a fixed-width file from mock-data included as prt of the project for testing

### Project Structure

![Project Structure](/images/struct.png)


File/Directory|Description
--------------|-------------
Dockerfile | Creates image for the container that hosts the project with requried dependencies
Makefile | Automates project build and tests - executed as part of the Docker file
Pipfile,Pipfile.lock | Part of `pipenv`
config | Contains configuration definitions for the fixed width parser and generator
data_generator | Module to generate fixed-width file from the mock dta availabe in `test/fixed_width_mock_data/`
data_parser | Module to parse fixed-width files
run.py | Python file to show the modules in action
test | Contains test data to run unit tests and generate fixed-width mock data



### Setup


#### Build the docker image
The Docker file will generate an image with the python project setup along with it's dependencies. To build the dockr image execute the following command inside the project folder ( root level )
`docker build -t fixed_width_parser:1.0 .`

The python project has been setup with `pipenv` for managing the dependencies. The `Pipfile` and `Pipfile.lock` files are generated by pipenv and should not be manipulated manually. The docker environment sets up pipenv with all its dependencies.

As a part of the build process the Docker file also executes a `Make` file. The make file handles setting up the python project using pipenv and also run the python unit tests. This is done as a part of building the image. Any test failure will cause the image build to fail.

#### Create a container  from the docker image
You can launch the container using either of the following commands:
`docker run -itd --name fixed_width_parser fixed_width_parser:1.0`
> You can also use access the files using `docker cp <containerId>:/file/path/within/container /host/path/target` or using `--mount` when launching the container

Once the container is in the running state you can login to the bash using the following command:<br>
`docker exec -it fixed_width_parser /bin/bash`

You should be in the home directort with the project files available there. An `ls` would give the following result,<br>
`Dockerfile  Makefile  Pipfile  Pipfile.lock  config  data_generator  data_parser  run.py  test`

Ideally, the virtual environment is already setup by the Makefile and you should be able to execute the `run.py` file. You can do that by running the following command,<br>
`pipenv run python run.py`<br>
You should have the following response ;<br>
> 2021-05-02 23:02:15,456 - FixedWidthParser - INFO - Fixed-Width source file location: /home/user_data.txt<br>
> 2021-05-02 23:02:15,515 - FixedWidthParser - INFO - CSV file location after parsing the fixed-width file: /home/user_data.csv<br>


<br>
Depending on how you launched your container ( w/ or w/o volume bind ) you should be able to see/access the above files in your host machine or your container.<br>

- /home/user_data.txt : Represents the fixed-width file generated from the mock data.This file is given as an input to the fixed-width parser - CSV file generator ( special characters in the first 2 records for testing encoding, Ridulfö and Gâbb - unsupported characters in cp1252 ). 
```
1~~~~Kelley~~~~~~~~~~~~~~Ridulf?~~~~~~~~~~~~~kridulfo0@jiathis.com~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Agender194.128.68.229~~true~~~~~~~~6.5~~~~~~~~~~~~~~~~Grand Cherokee~2004
2~~~~Molly~~~~~~~~~~~~~~~G?bb~~~~~~~~~~~~~~~~mgabb1@virginia.edu~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Male1.6.63.148~~~~~~true~~~~~~~0.80~~~~~~~~~~~~~~~~~~~~~~~~~Quest~1994
3~~~~Violet~~~~~~~~~~~~~~Portugal~~~~~~~~~~~~vportugal2@berkeley.edu~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Female17.87.151.98~~~~true~~~~~~~~1.0~~~~~~~~~~~~~~~~~~~~~~B-Series~2002
4~~~~Karissa~~~~~~~~~~~~~Righy~~~~~~~~~~~~~~~krighy3@intel.com~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Female111.232.218.150~true~~~~~~2.5.8~~~~~~~~~~~~~~~~~~~~~Outlander~2003
5~~~~Susie~~~~~~~~~~~~~~~Gapper~~~~~~~~~~~~~~sgapper4@springer.com~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Non-binary252.186.123.157~true~~~~~~~0.16~~~~~~~~~~~~~~~~Daewoo Lacetti~2006
6~~~~Peggi~~~~~~~~~~~~~~~Harrismith~~~~~~~~~~pharrismith5@businesswire.com~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Female11.240.76.73~~~~false~~~~~2.7.8~~~~~~~~~~~~~~~~~~~~~~~~Ranger~2004
7~~~~Somerset~~~~~~~~~~~~Sturgess~~~~~~~~~~~~ssturgess6@vimeo.com~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Genderqueer84.173.100.97~~~false~~~~~2.4.0~~~~~~~~~~~~~~~~~~~~~~~C-Class~1996
8~~~~Brana~~~~~~~~~~~~~~~Symon~~~~~~~~~~~~~~~bsymon7@behance.net~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Bigender249.160.32.126~~false~~~~~0.2.0~~~~~~~~~~~~~~~~~~~~~~1 Series~2011
9~~~~Janka~~~~~~~~~~~~~~~Toffoletto~~~~~~~~~~jtoffoletto8@sina.com.cn~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Genderqueer104.67.128.145~~true~~~~~~5.2.4~~~~~~~~~~~~~~~~~~~~~~~~~~Neon~2002
10~~~Karrie~~~~~~~~~~~~~~Tapton~~~~~~~~~~~~~~ktapton9@seattletimes.com~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Female96.216.118.33~~~true~~~~~~~2.40~~~~~~~~~~~~~~~~~~~~Pathfinder~2004
```
<br>

- /home/user_data.csv: Represents the CSV file created by parsing the above fixed width file.
```
id,first_name,last_name,email,gender,ip_address,status,app_version,car_model,car_model_year
1,Kelley,Ridulfö,kridulfo0@jiathis.com,Agender,194.128.68.229,true,6.5,Grand Cherokee,2004
2,Molly,Gâbb,mgabb1@virginia.edu,Male,1.6.63.148,true,0.80,Quest,1994
3,Violet,Portugal,vportugal2@berkeley.edu,Female,17.87.151.98,true,1.0,B-Series,2002
4,Karissa,Righy,krighy3@intel.com,Female,111.232.218.150,true,2.5.8,Outlander,2003
5,Susie,Gapper,sgapper4@springer.com,Non-binary,252.186.123.157,true,0.16,Daewoo Lacetti,2006
6,Peggi,Harrismith,pharrismith5@businesswire.com,Female,11.240.76.73,false,2.7.8,Ranger,2004
7,Somerset,Sturgess,ssturgess6@vimeo.com,Genderqueer,84.173.100.97,false,2.4.0,C-Class,1996
8,Brana,Symon,bsymon7@behance.net,Bigender,249.160.32.126,false,0.2.0,1 Series,2011
9,Janka,Toffoletto,jtoffoletto8@sina.com.cn,Genderqueer,104.67.128.145,true,5.2.4,Neon,2002
```