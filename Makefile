.PHONY: all init build test_package say_hello

all: init build test_package say_hello

init:
	@echo "Checking if pipenv is installed"
	@pipenv --version

build:
	@echo "Installing dependencies via pipenv"
	@pipenv install

test_package:
	@echo "Testing parser package"
	@cd test;python3 -m unittest test_parser.py

say_hello:
	@echo '##### Hello ! Dependencies have been installed and using pipenv and tests have been executed succesfully. You can run the file using the command -- `pipenv run python run.py` -- #####'