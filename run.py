'''
run.py

This file uses the modules for generating and parsing a fixed width file and show them in action
'''
from config.fixed_width_generator import generator_config
from config.fixed_width_parser import parser_config
from data_generator import generator
from data_parser import parser
import test
import json
import logging

try:
    ## Create or get the logger
    logger = logging.getLogger('FixedWidthParser')
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ## Set log level
    logger.setLevel(logging.DEBUG)

    ## Generate fixed_width file from source data in test folder
    fixed_width_source = generator.generate_data()
    logger.info("Fixed-Width source file location: " + fixed_width_source)

    ## Pull parsing config for parsing the Fixed-Width file generated above
    parsing_config = parser_config.CONFIG

    ## Generate CSV file from given fixed width file source
    csv_file_location = parser.generate_csv(
        fixed_width_source,
        parsing_config['Offsets'],
        parsing_config['ColumnNames'],
        parsing_config['Padding'],
        parsing_config['FixedWidthEncoding'],
        parsing_config['DelimitedEncoding']
    )

    logger.info("CSV file location after parsing the fixed-width file: "+csv_file_location)
except Exception as e:
    traceback.print_exc()
    # Any action to be taken on failure
   