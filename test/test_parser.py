'''
test_parser.py

Tests the expected results against the modules
'''
import sys
sys.path.insert(1, '../')
import unittest
import os
from config.fixed_width_parser import parser_config
from data_parser import parser

class FixedWidthParserTest(unittest.TestCase):
    
    def test_dataset_1(self):
        # Data set expected as an output when parsed by the parser
        expected_result = [
            "id,firstName,lastName,salary\n",
            "8A7111,John,Doe,80000\n",
            "8A7112,Mary,Cay,100000\n",
            "8A7113,Tom,Howard,600000\n",
            "8A7114,Liz,Taylor,700000\n"
        ]

        csv_file_location = parser.generate_csv(
            'test_data/fixedwidth_sample1.txt',
            [6,10,10,6],
            ['id','firstName','lastName','salary'],
            '~',
            'utf-8',
            'utf-8'
        )

        parsed_data = []
        with open(csv_file_location) as csv_file:
            parsed_data = csv_file.readlines()
        
        os.remove(csv_file_location)
        self.assertListEqual(parsed_data, expected_result)

    def test_dataset_2(self):

        # Data set expected as an output when parsed by the parser
        expected_result = [
            "id,status,assignment_unit,timestamp,user\n",
            "070300001,Assigned,13,2010-03-05 14:23:00,Sridar\n",
            "070600004,Open,13,2010-03-05 19:50:00,\n",
            "070600004,Resolved,13,2010-03-05 19:50:00,Niku\n"
        ]

        csv_file_location = parser.generate_csv(
            'test_data/fixedwidth_sample2.txt',
            [9,8,4,19,6],
            ['id','status','assignment_unit','timestamp','user'],
            ' ',
            'utf-8',
            'utf-8'
        )

        parsed_data = []
        with open(csv_file_location) as csv_file:
            parsed_data = csv_file.readlines()
        
        os.remove(csv_file_location)
        self.assertListEqual(parsed_data, expected_result)

if __name__ == '__main__':
    unittest.main()
