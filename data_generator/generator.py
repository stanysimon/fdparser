'''
generator.py

This function generates fixed-width file from the mock data it is made available in the test folder. This function makes use of the
FixedWidth project ( https://pypi.org/project/FixedWidth/ ) to do this.
Encoding is fixed to : cp1252 when generating the fixed width file.
'''
import sys
sys.path.insert(1, '../')
try:
    from fixedwidth import FixedWidth
except ImportError:
    from fixedwidth.fixedwidth import FixedWidth

import csv
import os
from copy import deepcopy
from config.fixed_width_generator import generator_config

def generate_data():
    fw_config = deepcopy(generator_config.CONFIG)
    fw_obj = FixedWidth(fw_config)

    with open('user_data.txt', mode='w',encoding='cp1252') as user_data:

        with open('./test/fixed_width_mock_data/mock_data.csv',encoding='utf-8') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                fw_obj.update(
                    id=row[0], 
                    first_name=row[1],
                    last_name=row[2], 
                    email=row[3], 
                    gender=row[4],
                    ip_address=row[5],
                    status=row[6], 
                    app_version=row[7], 
                    car_model=row[8],
                    car_model_year=row[9]
                )
                user_data.write(fw_obj.line)
    return os.path.abspath('user_data.txt')