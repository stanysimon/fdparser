FROM ubuntu:18.04
RUN apt-get -y update && apt-get -y install software-properties-common vim && \
    add-apt-repository -y universe && \
    add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get -y install python3.9 && apt-get -y install python3-pip &&\
    pip3 install --upgrade pip && \
    pip3 install pipenv && \
    apt-get -y install python3.9-venv
COPY . /home/
WORKDIR "/home/"
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN make
